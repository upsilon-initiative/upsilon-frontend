import sqlalchemy
import sqlalchemy.orm

TableBase = sqlalchemy.orm.declarative_base()


class User(TableBase):

    __tablename__ = "User"

    user_id = sqlalchemy.Column(
        sqlalchemy.Integer, autoincrement=True, primary_key=True
    )
    discord_id = sqlalchemy.Column(sqlalchemy.BigInteger, nullable=False)
    name = sqlalchemy.Column(sqlalchemy.String(), nullable=False)

    submission = sqlalchemy.orm.relationship(
        "Submission", back_populates="user", cascade="all, delete-orphan"
    )
    participant = sqlalchemy.orm.relationship(
        "ProjectParticipant",
        back_populates="user",
        cascade="all, delete-orphan",
    )


class Craft(TableBase):

    __tablename__ = "Craft"

    craft_id = sqlalchemy.Column(
        sqlalchemy.Integer, autoincrement=True, primary_key=True
    )
    submission_id = sqlalchemy.Column(
        sqlalchemy.Integer,
        sqlalchemy.ForeignKey("Submission.submission_id"),
        nullable=False,
    )
    name = sqlalchemy.Column(sqlalchemy.String(), nullable=False)
    categorisation = sqlalchemy.Column(sqlalchemy.String(), nullable=False)
    parent = sqlalchemy.Column(sqlalchemy.String(), nullable=False)
    situation = sqlalchemy.Column(sqlalchemy.String(), nullable=False)
    capacity = sqlalchemy.Column(sqlalchemy.Integer, nullable=False)

    submission = sqlalchemy.orm.relationship("Submission", back_populates="craft")
    kerbal = sqlalchemy.orm.relationship(
        "KerbalInCraft", back_populates="craft", cascade="all, delete-orphan"
    )


class KerbalInCraft(TableBase):

    __tablename__ = "KerbalInCraft"

    name = sqlalchemy.Column(sqlalchemy.String(), primary_key=True)
    craft_id = sqlalchemy.Column(
        sqlalchemy.Integer, sqlalchemy.ForeignKey("Craft.craft_id"), primary_key=True
    )

    craft = sqlalchemy.orm.relationship("Craft", back_populates="kerbal")


class Submission(TableBase):

    __tablename__ = "Submission"

    submission_id = sqlalchemy.Column(
        sqlalchemy.Integer, autoincrement=True, primary_key=True
    )
    project_id = sqlalchemy.Column(
        sqlalchemy.Integer, sqlalchemy.ForeignKey("Project.project_id"), nullable=True
    )
    user_id = sqlalchemy.Column(
        sqlalchemy.Integer, sqlalchemy.ForeignKey("User.user_id"), nullable=False
    )
    approved = sqlalchemy.Column(sqlalchemy.Boolean, nullable=False)
    positive_votes = sqlalchemy.Column(sqlalchemy.Integer, nullable=False)
    negative_votes = sqlalchemy.Column(sqlalchemy.Integer, nullable=False)
    message_id = sqlalchemy.Column(sqlalchemy.BigInteger, nullable=True)
    uses_dlc = sqlalchemy.Column(sqlalchemy.Boolean, nullable=False)

    user = sqlalchemy.orm.relationship("User", back_populates="submission")
    craft = sqlalchemy.orm.relationship(
        "Craft", back_populates="submission", cascade="all, delete-orphan"
    )
    project = sqlalchemy.orm.relationship("Project", back_populates="submission")
    mod = sqlalchemy.orm.relationship(
        "ModInSubmission",
        back_populates="submission",
        cascade="all, delete-orphan",
    )


class ModInSubmission(TableBase):

    __tablename__ = "ModInSubmission"

    name = sqlalchemy.Column(sqlalchemy.String(), primary_key=True)
    submission_id = sqlalchemy.Column(
        sqlalchemy.Integer,
        sqlalchemy.ForeignKey("Submission.submission_id"),
        primary_key=True,
    )
    enabled = sqlalchemy.Column(sqlalchemy.Boolean, nullable=False)

    submission = sqlalchemy.orm.relationship("Submission", back_populates="mod")


class Project(TableBase):

    __tablename__ = "Project"

    project_id = sqlalchemy.Column(
        sqlalchemy.Integer, autoincrement=True, primary_key=True
    )
    name = sqlalchemy.Column(sqlalchemy.String(), nullable=False)

    submission = sqlalchemy.orm.relationship("Submission", back_populates="project")
    participant = sqlalchemy.orm.relationship(
        "ProjectParticipant", back_populates="project", cascade="all, delete-orphan"
    )


class ProjectParticipant(TableBase):

    __tablename__ = "ProjectParticipant"

    project_id = sqlalchemy.Column(
        sqlalchemy.Integer,
        sqlalchemy.ForeignKey("Project.project_id"),
        primary_key=True,
    )
    user_id = sqlalchemy.Column(
        sqlalchemy.Integer, sqlalchemy.ForeignKey("User.user_id"), primary_key=True
    )
    role = sqlalchemy.Column(sqlalchemy.String(), nullable=False)

    project = sqlalchemy.orm.relationship("Project", back_populates="participant")
    user = sqlalchemy.orm.relationship("User", back_populates="participant")
