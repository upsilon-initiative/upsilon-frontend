# pylint: disable=logging-format-interpolation, c-extension-no-member
import copy
import logging
import multiprocessing.connection
import os
import shutil
import time

import flask
import flask_sqlalchemy
import natsort
import magic
import sfsutils
import sqlalchemy.orm.attributes
import ujson as json
import werkzeug

import craft_handler
from db_relations import (
    Submission,
    User,
    # Project,
    # ProjectParticipant,
    Craft,
    KerbalInCraft,
    ModInSubmission,
)

APP = flask.Flask(__name__)
APP.config.update(
    SESSION_COOKIE_SECURE=True,
    SESSION_COOKIE_HTTPONLY=True,
    SESSION_COOKIE_SAMESITE="Lax",
)


def create_sublogger(level, path):
    """Sets up a sublogger"""
    formatter = logging.Formatter(
        "%(asctime)s %(name)s %(process)d %(levelname)s %(message)s"
    )
    logger_handler = logging.FileHandler(path)
    logger_handler.setLevel(level)
    logger_handler.setFormatter(formatter)
    return logger_handler


ROOT_LOGGER = logging.getLogger()
ROOT_LOGGER.setLevel(logging.DEBUG)
os.makedirs("logs", exist_ok=True)
ROOT_LOGGER.addHandler(create_sublogger(logging.INFO, "logs/upsilon_main.log"))
LOGGER = logging.getLogger("upsilon_main")

with open("config.json", "r") as config_file:
    CONFIG = json.load(config_file)
    DATA_STORAGE_PATH = CONFIG["data_storage_path"]
    SUBMISSIONS_PATH = CONFIG["submissions_path"]
    IMAGES_PATH = CONFIG["images_path"]
    TEMP_SAVEFILE_PATH = CONFIG["temp_savefile_path"]
    # used for screenshot links in database
    SITE_LINK = CONFIG["site_link"]


SECRETS = {}
try:
    from flask_discord import DiscordOAuth2Session

    with open("secrets.json", "r") as secrets_file:
        SECRETS = json.load(secrets_file)
    APP.secret_key = SECRETS["app"].encode()
    APP.config["DISCORD_CLIENT_ID"] = SECRETS["discord_id"]
    APP.config["DISCORD_CLIENT_SECRET"] = SECRETS["discord_secret"]
    APP.config["DISCORD_REDIRECT_URI"] = SECRETS["discord_url"]
    DISCORD = DiscordOAuth2Session(APP)
    APP.config["SQLALCHEMY_DATABASE_URI"] = SECRETS["database_connect_string"]
except (FileNotFoundError, ImportError):
    APP.secret_key = b"qwerty"
    LOGGER.info("Using dummy discord / database configuration")

    class DiscordDummy:
        # pylint: disable=missing-docstring
        def __init__(self):
            self.authorized = True

        def fetch_user(self):
            return DiscordUser()

        def callback(self):
            pass

        def create_session(self, **kwargs):
            pass

    DISCORD = DiscordDummy()
    APP.config["SQLALCHEMY_DATABASE_URI"] = "sqlite://"


APP.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
DB = flask_sqlalchemy.SQLAlchemy(
    APP, session_options={"future": True}, engine_options={"future": True}
)


TEMPLATE_SFS = "upsilon_export.sfs"
SCREENSHOT_EXTENSIONS = {"png", "jpg", "jpeg"}
SCREENSHOT_MIMES = {"image/png", "image/jpeg"}


class ConnectionWrapper:
    # pylint: disable=missing-docstring
    """Connection wrapper"""

    def __init__(self, address=None, family=None, authkey=None):
        self.address = address
        self.family = family
        self.authkey = authkey
        self.connection = None
        if address is not None:
            self.open_connection()

    def open_connection(self):
        while True:
            try:
                self.connection = multiprocessing.connection.Client(
                    self.address, self.family, self.authkey
                )
            except Exception:
                LOGGER.error("IPC connect failed")
                time.sleep(5)
            else:
                LOGGER.info("IPC connect successful")
                return

    def send(self, data):
        if self.address is not None:
            LOGGER.debug("IPC send begin")
            while True:
                try:
                    LOGGER.debug("IPC send data")
                    self.connection.send(data)
                    return
                except ConnectionError:
                    LOGGER.error("IPC send failed")
                    self.open_connection()


if "bot_socket" in SECRETS:
    LOGGER.debug("Attempting to open ipc client")
    BOT_CONNECTION = ConnectionWrapper(
        CONFIG["socket_location"],
        family="AF_UNIX",
        authkey=SECRETS["bot_socket"].encode(),
    )
else:
    BOT_CONNECTION = ConnectionWrapper()


class DiscordUser:
    """Dummy discord user"""

    def __init__(
        self, name="Test", discriminator="0001", verified=True, discord_id=123456789
    ):
        self.name = name
        self.discriminator = discriminator
        self.verified = verified
        self.id = discord_id

    def __str__(self):
        return "{0}#{1}".format(self.name, self.discriminator)


class SessionStorage:
    """Stores user local information"""

    def __init__(self, session_id):
        self.folder = os.path.join(
            DATA_STORAGE_PATH, TEMP_SAVEFILE_PATH, str(session_id)
        )
        if not os.path.exists(self.folder):
            os.makedirs(self.folder)

    def remove_data(self, file, remove_folder=False):
        """Removes data from the specified file, and the whole session folder if specified"""
        if os.path.exists(os.path.join(self.folder, file)):
            os.remove(os.path.join(self.folder, file))
        if remove_folder:
            os.rmdir(self.folder)

    def write_data(self, data, file):
        """Writes data to the specified file"""
        with open(os.path.join(self.folder, file), "w") as data_file:
            json.dump(data, data_file)

    def read_data(self, file):
        """Reads data from the specified file"""
        with open(os.path.join(self.folder, file), "r") as data_file:
            # return json.load(data_file, object_pairs_hook=collections.OrderedDict)
            return json.load(data_file)


def filter_raw(craft):
    """Filters the raw key from the craft dictionary"""
    return {k: v for k, v in craft.items() if k != "raw"}


def get_submission_paths(submission_id, craft_id=None):
    base = os.path.join(DATA_STORAGE_PATH, SUBMISSIONS_PATH, str(submission_id))
    out = {"base": base, "images": os.path.join(base, IMAGES_PATH)}
    if craft_id is not None:
        out["craft"] = os.path.join(base, "{0}.json".format(craft_id))
    return out


def make_error_response(message: str, status_code: int) -> flask.Response:
    return flask.make_response(({"error": message}, status_code))


def make_json_response(obj: object, status_code: int = 200) -> flask.Response:
    data = json.dumps(obj)
    response = flask.make_response((obj, status_code))
    response.content_type = "application/json"
    return data


@APP.route("/")
def home_page():
    """Home page"""
    options = {}
    if DISCORD.authorized:
        user = DISCORD.fetch_user()
        LOGGER.info("Homepage from {0}".format(user))
        options["discord_name"] = user.name
    else:
        user = None
    options["user"] = user
    options["discord_url"] = CONFIG["discord_url"]
    size_limits = {"": {"default": True, "name": "No limit"}}
    size_limits.update(
        {
            key: {"default": False, "name": value}
            for key, value in craft_handler.SIZES.items()
            if key not in ("N", "E")
        }
    )
    options["size_limits"] = size_limits
    return flask.render_template("index.html", **options)


@APP.route("/submissions/stats", methods=["GET"])
def submission_stats():
    """Submission statistics endpoint"""
    categorisations = list(craft_handler.SIZES.keys())[1:]
    dlc_types = ("nodlc", "dlc")
    bodies = {}
    for body in craft_handler.SOLAR_SYSTEM:
        body_dict = {}
        for category in dlc_types:
            body_dict[category] = {x: 0 for x in categorisations}
        bodies[body] = body_dict
    crafts = (
        DB.session.execute(
            sqlalchemy.select(Craft)
            .filter(Craft.submission_id == Submission.submission_id)
            .filter(Submission.approved == True)
        )
        .scalars()
        .all()
    )
    for craft in crafts:
        size = craft.categorisation[0]
        category = "dlc" if craft.submission.uses_dlc else "nodlc"
        bodies[craft.parent][category][size] += 1
    for body, body_data in bodies.items():
        for category in dlc_types:
            new_sizes = {"limits": {}, "total": sum(body_data[category].values())}
            for size in body_data[category]:
                size_index = categorisations.index(size)
                new_sizes["limits"][size] = sum(
                    v
                    for k, v in body_data[category].items()
                    if size_index >= categorisations.index(k)
                )
            bodies[body][category] = new_sizes
    return bodies


@APP.route("/submissions/<int:submission_id>/", methods=["GET"])
def present_submission(submission_id):
    """Submission viewing page"""
    submission = DB.session.execute(
        sqlalchemy.select(Submission).filter_by(submission_id=submission_id)
    ).scalar_one_or_none()
    if submission is None:
        return flask.render_template("error.html", error_message="Submission not found")

    images_path = os.path.join(
        DATA_STORAGE_PATH, SUBMISSIONS_PATH, str(submission_id), IMAGES_PATH
    )
    files = natsort.natsorted(os.listdir(images_path))

    return flask.render_template("submission.html", submission=submission, files=files)


@APP.route("/logout/")
def logout_page():
    """Discord logout page"""
    if not DISCORD.authorized:
        return (
            flask.render_template("error.html", error_message="Already logged out"),
            400,
        )
    DISCORD.revoke()
    return flask.redirect(flask.url_for("home_page"))


@APP.route("/submissions/<int:submission_id>", methods=["PATCH"])
def update_craft(submission_id):
    """Endpoint for submission updates"""
    if not DISCORD.authorized:
        return make_error_response(DISCORD_AUTH_ERROR, 401)
    user = DISCORD.fetch_user()
    post_data = flask.request.json
    submission = DB.session.execute(
        sqlalchemy.select(Submission).filter_by(submission_id=submission_id)
    ).scalar_one_or_none()
    if submission is None:
        return make_error_response("Submission not found", 404)
    if submission.user.discord_id != user.id:
        return make_error_response("Insufficient rights to modify this submission", 403)

    bot_actions = []
    # sanitise craft name
    if "name" in post_data:
        craft_name = werkzeug.utils.secure_filename(post_data["name"])
        if not craft_name:
            DB.session.rollback()
            return make_error_response("Craft name cannot be blank", 400)
        submission.craft[0].name = craft_name  # XXX just selecting first craft
        bot_actions.append(
            dict(
                action="rename_submission",
                name=craft_name,
                message_id=submission.message_id,
            )
        )

    DB.session.commit()
    for action in bot_actions:
        BOT_CONNECTION.send(action)
    return "", 204


@APP.route("/submissions/<int:submission_id>", methods=["DELETE"])
def delete_craft(submission_id):
    """Endpoint for submission deletion"""
    if not DISCORD.authorized:
        return make_error_response(DISCORD_AUTH_ERROR, 401)
    user = DISCORD.fetch_user()
    submission = DB.session.execute(
        sqlalchemy.select(Submission).filter_by(submission_id=submission_id)
    ).scalar_one_or_none()
    if submission is None:
        return make_error_response("Submission not found (already deleted?)", 404)
    if submission.user.discord_id != user.id:
        return make_error_response(
            "Insufficient rights to modify this submission",
            403,
        )
    LOGGER.info("Deleting submission {0} from {1}".format(submission_id, user))
    shutil.rmtree(get_submission_paths(submission.submission_id)["base"])
    DB.session.delete(submission)
    DB.session.commit()
    BOT_CONNECTION.send(
        dict(action="delete_submission", message_id=submission.message_id)
    )
    return "", 204


@APP.route("/deliver/craft", methods=["POST"])
def handle_submission():
    """Endpoint for save file handling"""
    if not DISCORD.authorized:
        return make_error_response(DISCORD_AUTH_ERROR, 401)
    user = DISCORD.fetch_user()
    session = SessionStorage(user.id)
    data = flask.request.files["sfsfile"].read().decode()
    data = data.replace("\r", "").replace("\n ", "\n").strip()
    try:
        save = sfsutils.parse_savefile(data, sfs_is_path=False)
    except Exception:
        LOGGER.exception("SFS parse failed")
        LOGGER.error("Error parsing safefile from {0}".format(user))
        return make_error_response(
            "Error parsing savefile - the savefile format is invalid. "
            "Check that the file is not corrupted or modified incorrectly",
            400,
        )
    try:
        processed_crafts = craft_handler.process_savefile(save)
    except Exception:
        LOGGER.exception("Craft parse failed")
        LOGGER.warning("Error parsing crafts from {0}".format(user))
        return make_error_response(
            "Error checking savefile: Please request to have a staff member check your save",
            400,
        )
    LOGGER.warning("Warnings: {0}".format(processed_crafts["warnings"]))
    if processed_crafts["exit_invalid"]:
        return make_json_response(processed_crafts)
    session.write_data(processed_crafts, "temp.json")
    for pid, craft in processed_crafts["crafts"].items():
        if craft["invalidation_reasons"]:
            LOGGER.warning(
                "Craft {0} invalid: {1}".format(
                    craft["name"], craft["invalidation_reasons"]
                )
            )
        del processed_crafts["crafts"][pid]["raw"]
    del processed_crafts["ROSTER"]
    return make_json_response(processed_crafts)


@APP.route("/submissions", methods=["GET"])
def list_submissions():
    if not DISCORD.authorized:
        return make_error_response(DISCORD_AUTH_ERROR, 401)
    user = DISCORD.fetch_user()
    submissions = (
        DB.session.execute(
            sqlalchemy.select(Submission)
            .filter(User.discord_id == user.id)
            .filter(User.user_id == Submission.user_id)
        )
        .scalars()
        .all()
    )

    output = {}
    for submission in submissions:
        first_craft = submission.craft[0]
        output[submission.submission_id] = {
            "name": first_craft.name,
            "categorisation": first_craft.categorisation,
            "parent": first_craft.parent,
            "situation": first_craft.situation,
            "dlc": submission.uses_dlc,
            "positive_votes": submission.positive_votes,
            "negative_votes": submission.negative_votes,
            "approved": submission.approved,
        }
    return output


@APP.route("/submissions", methods=["POST"])
def handle_screenshots():
    """Endpoint for screenshot handling"""
    if not DISCORD.authorized:
        return make_error_response(DISCORD_AUTH_ERROR, 401)
    user = DISCORD.fetch_user()
    session = SessionStorage(user.id)
    data = flask.request.files.getlist("screenshots[]")
    if not data:
        return make_error_response("Must upload 1 or more screenshots", 400)
    secure_filenames = []
    total_size = 0
    for index, file in enumerate(data):
        filename = werkzeug.utils.secure_filename(file.filename)
        if filename.count(".") != 1:
            return make_error_response(
                "File {0} has an invalid name".format(filename), 400
            )
        name, extension = filename.split(".")
        extension = extension.lower()
        if extension not in SCREENSHOT_EXTENSIONS:
            return make_error_response(
                "File {0} has an invalid name".format(filename), 400
            )
        filename = ".".join((name, extension))
        secure_filenames.append(filename)
        if magic.from_buffer(file.read(1024), mime=True) not in SCREENSHOT_MIMES:
            return make_error_response(
                "File {0} appears to be of invalid type".format(filename),
                400,
            )
        file.stream.seek(0, os.SEEK_END)
        file_length = file.stream.tell()
        if file_length > 8 * 1024**2:
            return make_error_response("File {0} is >8MB".format(filename), 400)
        total_size += file_length
        file.stream.seek(0)
    if total_size > 50 * 1024**2:
        return make_error_response("Total file size is >50MB", 400)
    if "submission_id" in flask.request.form:
        submission = DB.session.execute(
            sqlalchemy.select(Submission).filter_by(
                submission_id=flask.request.form["submission_id"]
            )
        ).scalar_one_or_none()
        if submission is None:
            return make_error_response("Submission not found", 404)
        if submission.user.discord_id != user.id:
            return make_error_response(
                "Insufficient rights to modify this submission",
                403,
            )
        images_path = get_submission_paths(submission.submission_id)["images"]
        for file in os.listdir(images_path):
            os.remove(os.path.join(images_path, file))
    else:
        craft_pid = flask.request.form["craft_pid"]
        processed_crafts = session.read_data("temp.json")
        craft_data = processed_crafts["crafts"][craft_pid]
        # sanitise craft name
        new_craft_name = werkzeug.utils.secure_filename(craft_data["name"])
        if not new_craft_name:
            return make_error_response("Craft name cannot be blank", 400)
        craft_data["name"] = new_craft_name
        # session.remove_data("temp.json", remove_folder=True)
        if craft_data["invalidation_reasons"]:
            return make_error_response("Invalid craft submitted", 400)
        db_user = DB.session.execute(
            sqlalchemy.select(User).filter_by(discord_id=user.id)
        ).scalar_one()
        submission = Submission(
            user_id=db_user.user_id,
            approved=False,
            positive_votes=0,
            negative_votes=0,
            uses_dlc=craft_data["dlc"],
        )
        submission.mod = [
            ModInSubmission(name=name, enabled=data["enabled"])
            for name, data in processed_crafts["warnings"]["mods"].items()
        ]
        craft = Craft(
            name=new_craft_name,
            categorisation=craft_data["categorisation"],
            parent=craft_data["parent_name"],
            situation=craft_data["situation"],
            capacity=craft_data["kerbals"]["capacity"],
        )
        craft.kerbal = [
            KerbalInCraft(name=name) for name in craft_data["kerbals"]["crew"]
        ]

        submission.craft.append(craft)

        DB.session.add(submission)
        # flushing allows the submission id to be assigned without committing it to the database
        DB.session.flush()
        paths = get_submission_paths(
            submission.submission_id, submission.craft[0].craft_id
        )
        images_path = paths["images"]
        os.makedirs(images_path)
        kerbals = filter_roster(
            processed_crafts["ROSTER"], craft_data["kerbals"]["crew"]
        )
        craft_data["raw"]["KERBAL"] = kerbals
        with open(paths["craft"], "w") as craft_file:
            json.dump(craft_data["raw"], craft_file)
    for index, file in enumerate(data):
        filename = secure_filenames[index]
        file.save(os.path.join(images_path, filename))
    DB.session.commit()
    if "submission_id" not in flask.request.form:  # don't post screenshot reuploads
        BOT_CONNECTION.send(
            dict(action="post_submission", submission_id=submission.submission_id)
        )
    return "", 204


def filter_roster(roster, kerbals):
    out = []
    for savefile_kerbal in roster["KERBAL"]:
        if savefile_kerbal["name"] in kerbals:
            out.append(savefile_kerbal)
    return out


@APP.route("/export")
def export_sfs():
    """Exports crafts as an sfs file for download"""
    # LOGGER.info("Export begin")
    categorisations = list(craft_handler.SIZES.keys())[1:]
    args = flask.request.args.to_dict()
    # bodies = craft_handler.SOLAR_SYSTEM
    selected_bodies = args["body"].split(",")
    dlc = args["dlc"] == "true"
    limit = args.get("limit", None)
    if limit is not None:
        limit = categorisations.index(limit)
    valid_dlc = [False]
    if dlc:
        valid_dlc.append(True)
    crafts = (
        DB.session.execute(
            sqlalchemy.select(Craft)
            .filter(Craft.submission_id == Submission.submission_id)
            .filter(Submission.approved == True)
            .filter(Submission.uses_dlc.in_(valid_dlc))
            .filter(Craft.parent.in_(selected_bodies))
        )
        .scalars()
        .all()
    )
    # LOGGER.info(selected_bodies)
    # LOGGER.info("Queried crafts")
    selected_crafts = []
    for craft in crafts:
        if limit is not None:
            if categorisations.index(craft.categorisation[0]) > limit:
                continue
        selected_crafts.append(craft)
    craft_files = []
    template = sfsutils.parse_savefile(TEMPLATE_SFS)
    dummy_kerbal_template = template["GAME"]["ROSTER"]["KERBAL"][0]
    kerbals = []
    for craft in selected_crafts:
        craft_path = get_submission_paths(
            craft.submission.submission_id, craft.craft_id
        )["craft"]
        with open(craft_path, "r") as craft_file_handle:
            # data = json.load(craft_file_handle, object_pairs_hook=collections.OrderedDict)
            data = json.load(craft_file_handle)
            data["name"] = "{0} - {1}".format(craft.categorisation, craft.name)
            if "KERBAL" in data:
                kerbals.extend(data.pop("KERBAL"))
            else:
                for kerbal in craft.kerbal:
                    dummy_kerbal = copy.deepcopy(dummy_kerbal_template)
                    dummy_kerbal["name"] = kerbal.name
                    kerbals.append(dummy_kerbal)
            craft_files.append(data)
    # LOGGER.info("Loaded data")
    template["GAME"]["FLIGHTSTATE"]["VESSEL"] = craft_files
    template["GAME"]["FLIGHTSTATE"]["UT"] = str(
        max(
            float(vessel["lastUT"])
            for vessel in template["GAME"]["FLIGHTSTATE"]["VESSEL"]
        )
    )
    template["GAME"]["ROSTER"]["KERBAL"].extend(kerbals)
    return sfsutils.writeout_savefile(template)


@APP.route("/discord/login/")
def login():
    """Discord session creation redirect"""
    return DISCORD.create_session(scope=["identify"])


@APP.route("/discord/callback")
def callback():
    """Discord session creation callback"""
    DISCORD.callback()
    if DISCORD.authorized:
        user = DISCORD.fetch_user()
        user_entry = DB.session.execute(
            sqlalchemy.select(User).filter_by(discord_id=user.id)
        ).scalar_one_or_none()
        if user_entry is None:
            user_entry = User(discord_id=user.id, name=user.name)
            DB.session.add(user_entry)
            DB.session.commit()
        elif user_entry.name != user.name:
            user_entry.name = user.name
            DB.session.commit()
    return flask.redirect(flask.url_for("home_page"))


DISCORD_AUTH_ERROR = "Must be discord authorised"

LOGGER.info("Server booted")
