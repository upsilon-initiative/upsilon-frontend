"use strict"
var ns = new function () {
    var currentSubmissionsDataGlobal = null
    var newSubmissionDataGlobal = null
    var downloadCraftData = null
    var currentlyDownloading = false

    this.setBodyOnError = function (content) {
        document.body.innerHTML = `
        <div id="main" class="uk-section-secondary" style="margin-left: 8rem; position: relative;" uk-height-viewport>
            <h1 class="uk-heading-large" style="font-family: 'Roboto Mono', monospace; font-weight:200;">error</h1>
            <div id="replace-content" class="uk-section uk-padding-top">
                ${content}
            </div>
        <button onclick="location.assign('/')" class="uk-button uk-button-primary uk-align-left">Homepage</button>
        </div>`
    }
    this.processJSONResponse = function (xhttp) {
        try {
            return JSON.parse(xhttp.response)
        } catch {
            return null
        }
    }
    this.updateSubmissions = function () {
        let xhttp = new XMLHttpRequest()
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4) {

                let response = ns.processJSONResponse(xhttp)

                if (this.status === 200) {
                    currentSubmissionsDataGlobal = response
                    let out = []
                    for (const [submissionId, submissionData] of Object.entries(currentSubmissionsDataGlobal)) {
                        out.push({ "text": `${submissionData["name"]} - ${submissionData["categorisation"]}`, "value": submissionId })
                    }
                    ns.setSelectOptions(document.getElementById("submission_select"), out)
                    ns.craftDetails("submission")
                }
                else {
                    if (response === null) {
                        ns.setBodyOnError(xhttp.response)
                    } else {
                        alert(response["error"])
                    }
                }
            }
        }
        xhttp.open("GET", "/submissions", true)
        xhttp.send()
    }
    this.setSelectOptions = function (select, options) {
        while (select.length > 1) { // leave the first disabled element in
            select.remove(1)
        }
        for (let item of options) {
            let element = document.createElement("option")
            element.text = item["text"]
            element.value = item["value"]
            select.add(element)
        }
    }


    this.newSavefile = function () {
        let accordion = UIkit.accordion("#upload_accordion")
        let sfsFile = document.getElementById("upload_sfs").files[0]
        if (!sfsFile.name.endsWith(".sfs")) {
            alert("Only sfs files can be uploaded")
            return
        }

        let status = document.getElementById("sfs_upload_status")
        let icon = status.getElementsByTagName("span")[0]
        let fileFormData = new FormData()
        fileFormData.append("sfsfile", sfsFile)

        let xhttp = new XMLHttpRequest()
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4) {

                let response = ns.processJSONResponse(xhttp)

                if (this.status === 200) {
                    if (response["exit_invalid"]) {
                        ns.updateIcon(icon, "warning")
                        status.childNodes[2].nodeValue = "Unprocessable mods"
                        accordion.toggle(1)
                    } else {
                        ns.updateIcon(icon, "check")
                        status.childNodes[2].nodeValue = `File loaded (${sfsFile.name})`
                        accordion.toggle(2)
                    }
                    ns.setModInfo(response)
                    ns.setCraftInfo(response)
                }
                else {
                    if (response === null) {
                        ns.setBodyOnError(xhttp.response)
                    } else {
                        ns.updateIcon(icon, "close")
                        status.childNodes[2].nodeValue = response["error"]
                        ns.setModInfo(null)
                        ns.setCraftInfo(null)
                        alert(response["error"])
                    }
                }
            }
        }
        xhttp.upload.onprogress = function (ev) {
            let percentLoaded = Math.round((ev.loaded / ev.total) * 100)
            if (percentLoaded === 100) {
                ns.updateIcon(icon, "clock")
                status.childNodes[2].nodeValue = "Server processing file"
            } else {
                status.childNodes[2].nodeValue = `Uploading file (${percentLoaded}%)`
            }
        }
        xhttp.open("POST", "/deliver/craft", true)
        xhttp.send(fileFormData)
        ns.updateIcon(icon, "upload")
        status.childNodes[2].nodeValue = "Uploading file"

    }
    this.setModInfo = function (saveFileData) {
        let tableBody = document.getElementById("mod_table").getElementsByTagName("tbody")[0]
        tableBody.innerHTML = ""

        let status = {}
        for (const indicator of ["savefile", "active", "version", "difficulty"]) {
            let container = document.getElementById(`mod_${indicator}_status`)
            status[indicator] = [container, container.getElementsByTagName("span")[0]]
        }
        if (saveFileData === null) {
            for (const [_, icon] of Object.values(status)) {
                ns.updateIcon(icon, "minus")
            }
            status["savefile"][0].childNodes[2].nodeValue = "No savefile active"
            status["active"][0].childNodes[2].nodeValue = "No mod info"
            status["version"][0].childNodes[2].nodeValue = "No version info"
            status["difficulty"][0].childNodes[2].nodeValue = "No difficulty info"
            return
        }
        let container, icon;

        [container, icon] = status["savefile"]
        if (saveFileData["exit_invalid"]) {
            ns.updateIcon(icon, "minus")
            container.childNodes[2].nodeValue = "Invalid savefile"
        } else {
            ns.updateIcon(icon, "plus")
            container.childNodes[2].nodeValue = "Savefile OK"
        }
        let saveFileWarnings = saveFileData.warnings;

        [container, icon] = status["active"]
        let mods_valid = true
        for (const [name, modData] of Object.entries(saveFileWarnings["mods"])) {
            let row = tableBody.insertRow()
            const data = [
                name,
                modData["enabled"] ? "Yes" : "No",
                modData["allowed"] ? "Yes" : "No"
            ]
            for (let cellData of data) {
                let cell = row.insertCell()
                cell.appendChild(document.createTextNode(cellData))
            }

            if (!modData["allowed"] && modData["enabled"]) {
                row.style = "color:red"
                mods_valid = false
            }
        }
        if (mods_valid) {
            ns.updateIcon(icon, "check")
            container.childNodes[2].nodeValue = "Mods all valid"
        } else {
            ns.updateIcon(icon, "close")
            container.childNodes[2].nodeValue = "Non-whitelisted mods are active"
        };

        [container, icon] = status["version"]
        if (saveFileWarnings["settings"]["version"]) {
            ns.updateIcon(icon, "close")
            container.childNodes[2].nodeValue = "KSP out of date"
        } else {
            ns.updateIcon(icon, "check")
            container.childNodes[2].nodeValue = "KSP version OK"
        };

        [container, icon] = status["difficulty"]
        if (saveFileWarnings["settings"]["difficulty"]) {
            ns.updateIcon(icon, "warning")
            container.childNodes[2].nodeValue = "Difficulty not set to normal"
        } else {
            ns.updateIcon(icon, "check")
            container.childNodes[2].nodeValue = "Difficulty on normal"
        }

    }
    this.setCraftInfo = function (saveFileData) {
        let craftSelect = document.getElementById("upload_select")
        if (saveFileData === null || saveFileData["exit_invalid"]) {
            newSubmissionDataGlobal = null
            ns.setSelectOptions(craftSelect, [])
            ns.craftDetails("upload")
            return
        }
        newSubmissionDataGlobal = saveFileData["crafts"]
        let out = []
        for (const [craftId, craftData] of Object.entries(newSubmissionDataGlobal)) {
            out.push({ "text": `${craftData["name"]} - ${craftData["categorisation"]}`, "value": craftId })
        }
        ns.setSelectOptions(craftSelect, out)
        ns.craftDetails("upload")
    }

    this.dropScreenshots = function (reupload) {
        let mode
        if (reupload) {
            mode = "submission"
        } else {
            mode = "upload"
        }
        let screenshots = document.getElementById(`${mode}_screenshots`)
        let status = document.getElementById(`${mode}_screenshots_status`)
        let icon = status.getElementsByTagName("span")[0]
        let submitButton = document.getElementById(`${mode}_button`)
        let selectedCraft = document.getElementById(`${mode}_select`).value

        let fail = function (text) {
            ns.updateIcon(icon, "close")
            status.childNodes[2].nodeValue = text
            submitButton.disabled = true
        }

        if (selectedCraft === "") {
            return fail("No craft selected")
        } else if (!reupload && newSubmissionDataGlobal[selectedCraft]["invalidation_reasons"].length) {
            return fail("Craft selected is invalid")
        }

        let totalSize = 0
        for (let file of screenshots.files) {
            if (!["image/png", "image/jpeg"].includes(file.type)) {
                return fail("Bad file type")
            }
            if (file.size > 8 * 1024 ** 2) {
                return fail("Single file size >8MiB")
            }
            totalSize += file.size
        }
        if (totalSize > 50 * 1024 ** 2) {
            return fail("Total file size >50MiB")
        }
        if (screenshots.files.length === 0) {
            return fail("Must upload 1 or more screenshots")
        }
        ns.updateIcon(icon, "check")
        status.childNodes[2].nodeValue = `OK, ${screenshots.files.length} screenshots loaded`
        submitButton.disabled = false
    }
    this.submitScreenshots = function (reupload) {
        let mode
        if (reupload) {
            mode = "submission"
        } else {
            mode = "upload"
        }
        let screenshots = document.getElementById(`${mode}_screenshots`)
        let status = document.getElementById(`${mode}_screenshots_status`)
        let icon = status.getElementsByTagName("span")[0]
        let submitButton = document.getElementById(`${mode}_button`)
        let craftSelect = document.getElementById(`${mode}_select`)
        let fileFormData = new FormData()

        submitButton.disabled = true

        if (!reupload && newSubmissionDataGlobal[craftSelect.value]["situation"] === "LANDED") {
            if (!confirm(`The craft you have chosen is currently landed. Please confirm that you ` +
                `used the "High" terrain detail setting, as otherwise your craft will ` +
                `clip into the the ground when others try to load it`)) {
                return
            }
        }
        for (let file of screenshots.files) {
            fileFormData.append('screenshots[]', file);
        }
        if (reupload) {
            fileFormData.append("submission_id", craftSelect.value)
        } else {
            fileFormData.append("craft_pid", craftSelect.value)
        }


        let xhttp = new XMLHttpRequest()
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4) {
                let response = ns.processJSONResponse(xhttp)

                if (reupload) {
                    document.getElementById("resubmit_screenshots").hidden = true
                }
                submitButton.disabled = false
                if (this.status === 204) {
                    ns.updateSubmissions()
                    if (!reupload) {
                        UIkit.accordion("#upload_accordion").toggle(0)
                        craftSelect.selectedIndex = 0 // will trigger cascading reflow
                    }
                    screenshots.files = []
                }
                else {
                    if (response === null) {
                        ns.setBodyOnError(xhttp.response)
                    } else {
                        ns.updateIcon(icon, "close")
                        status.childNodes[2].nodeValue = response["error"]
                        alert(response["error"]) // will be seen on reupload
                    }
                }
            }
        }
        xhttp.upload.onprogress = function (ev) {
            let percentLoaded = Math.round((ev.loaded / ev.total) * 100)
            if (percentLoaded === 100) {
                ns.updateIcon(icon, "clock")
                status.childNodes[2].nodeValue = "Server processing submission"
            } else {
                status.childNodes[2].nodeValue = `Uploading screenshots (${percentLoaded}%)`
            }

        }
        xhttp.open("POST", "/submissions", true)
        xhttp.send(fileFormData)
        ns.updateIcon(icon, "upload")
        status.childNodes[2].nodeValue = "Uploading screenshots"
    }


    this.craftDetails = function (mode) {
        let dataGlobal
        if (mode === "upload") {
            dataGlobal = newSubmissionDataGlobal
        } else if (mode === "submission") {
            dataGlobal = currentSubmissionsDataGlobal
        } else {
            console.error("Unexpected craft details mode")
            return
        }
        let currentCraft = document.getElementById(`${mode}_select`).value

        let set_functions = {
            "name": (el) => { el.textContent = craftData["name"] },
            "categorisation": (el) => { el.textContent = craftData["categorisation"] },
            "location": (el) => { el.textContent = craftData["parent_name"] },
            "situation": (el) => { el.textContent = craftData["situation"] },
            "dlc": (el) => { el.textContent = craftData["dlc"] ? "Yes" : "No" },
            "votes": (el) => { el.textContent = `Positive: ${craftData["positive_votes"]} Negative: ${craftData["negative_votes"]}` },
            "status": (el) => { el.textContent = craftData["approved"] ? "Approved" : "Not approved" },
            "page": (el) => { el.href = `/submissions/${currentCraft}/` },
            "valid": (el) => { el.textContent = craftData["invalidation_reasons"].length ? `No: ${craftData["invalidation_reasons"].join(", ")}` : "Yes" }
        }
        let clear_functions = {
            "name": (el) => { el.textContent = "" },
            "categorisation": (el) => { el.textContent = "" },
            "location": (el) => { el.textContent = "" },
            "situation": (el) => { el.textContent = "" },
            "dlc": (el) => { el.textContent = "" },
            "votes": (el) => { el.textContent = "" },
            "status": (el) => { el.textContent = "" },
            "page": (el) => { el.removeAttribute("href") },
            "valid": (el) => { el.textContent = "" }
        }
        let craftData
        let functions
        if (currentCraft === "") {
            craftData = null
            functions = clear_functions
        } else {
            craftData = dataGlobal[currentCraft]
            functions = set_functions
        }

        for (const [key, value] of Object.entries(functions)) {
            let element = document.getElementById(`${mode}_${key}`)
            if (element != null) {
                value(element)
            }
        }

        ns.dropScreenshots(mode === "submission")
        // if (mode === "upload") {
        //     // let button = document.getElementById("upload_craft_next")
        //     // button.disabled = (currentCraft === "" || craftData["invalidation_reasons"])
        // }
        if (mode === "submission") {
            let buttons = document.getElementById("submission_manage_buttons").getElementsByTagName("button")
            for (let button of buttons) {
                button.disabled = (currentCraft === "")
            }
        }

    }
    this.unhide = function (id) {
        document.getElementById(id).hidden = false
    }
    this.deleteSubmission = function () {
        let currentCraft = document.getElementById("submission_select").value
        if (currentCraft === "") {
            alert("No craft selected")
            return
        }
        let delete_confirm = confirm("Permanently delete submission?")
        if (!delete_confirm) {
            return
        }
        let xhttp = new XMLHttpRequest()
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4) {
                let response = ns.processJSONResponse(xhttp)
                if (this.status === 204) {
                    ns.updateSubmissions()
                } else {
                    if (response === null) {
                        ns.setBodyOnError(xhttp.response)
                    } else {
                        alert(response["error"])
                        ns.updateSubmissions()
                    }
                }
            }
        }
        xhttp.open("DELETE", `/submissions/${currentCraft}`, true)
        xhttp.send()
    }
    this.renameSubmission = function () {
        let currentCraft = document.getElementById("submission_select").value
        if (currentCraft === "") {
            alert("No craft selected")
            return
        }
        let newName = document.getElementById("submission_new_name")
        let xhttp = new XMLHttpRequest()
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4) {
                document.getElementById("rename_submission").hidden = true
                newName.value = ""
                let response = ns.processJSONResponse(xhttp)
                if (this.status === 204) {
                    ns.updateSubmissions()
                } else {
                    if (response === null) {
                        ns.setBodyOnError(xhttp.response)
                    } else {
                        alert(response["error"])
                        ns.updateSubmissions()
                    }
                }
            }
        }
        xhttp.open("PATCH", `/submissions/${currentCraft}`, true)
        xhttp.setRequestHeader("Content-Type", "application/json")
        xhttp.send(JSON.stringify({ "name": newName.value }))
    }

    this.updateIcon = function (element, text) {
        text = "icon:" + text
        let attrs = element.getAttribute("uk-icon");
        if (attrs.includes(";")) {
            attrs = attrs.split(";")
            attrs[0] = text;
            element.setAttribute("uk-icon", attrs.join(";"));
        } else {
            element.setAttribute("uk-icon", text)
        }
    }

    this.getSelectedBodies = function () {
        let container = document.getElementById("craft_checkbox_container")
        let checkboxes = container.querySelectorAll('input[type="checkbox"]')
        let out = []
        for (let checkbox of checkboxes) {
            if (checkbox.checked) {
                out.push(checkbox.nextSibling.textContent)
            }
        }
        return out
    }

    this.downloadSaveFile = function () {
        let status = document.getElementById("download_status")
        let icon = status.getElementsByTagName("span")[0]
        let button = document.getElementById("download_button")
        let limit = document.getElementById("size_limit_selector").value
        let dlc = document.getElementById("include_dlc").checked

        let selectedBodies = ns.getSelectedBodies()

        if (!selectedBodies.length) {
            alert("You must select a body")
            return
        }
        let downloadStatus = ns.downloadDetails()
        if (downloadStatus === null) {
            alert("Download not yet ready, please wait")
            return
        }
        if (downloadStatus === 0) {
            alert("No crafts to download")
            return
        }
        button.disabled = true
        currentlyDownloading = true
        status.childNodes[1].nodeValue = "Preparing file - please wait"
        ns.updateIcon(icon, "database")
        let downloadUrl = "/export?body=" + selectedBodies.join(",") + "&dlc=" + dlc

        if (limit !== "") {
            downloadUrl += "&limit=" + limit
        }

        let xhttp = new XMLHttpRequest()
        xhttp.onreadystatechange = function () {
            console.log(this.readyState)
            if (this.readyState === 3) {
                status.childNodes[1].nodeValue = "Downloading"
                ns.updateIcon(icon, "download")
            } else if (this.readyState === 4) {
                currentlyDownloading = false
                if (this.status === 200) {
                    let a = document.createElement("a")
                    a.download = "upsilon_export.sfs"
                    a.href = window.URL.createObjectURL(xhttp.response)
                    document.body.appendChild(a)
                    a.click()
                    document.body.removeChild(a)
                    ns.downloadDetails()
                }
                else {
                    ns.updateIcon(icon, "close")
                    status.childNodes[1].nodeValue = "Failed"
                    console.log("Unexpected response code")
                    alert("Error downloading savefile")
                }
                button.disabled = false
            }
        }
        xhttp.open("GET", downloadUrl, true)
        xhttp.responseType = "blob"
        xhttp.send()
    }
    this.downloadDetails = function () {
        if (downloadCraftData === null || currentlyDownloading) {
            return null
        }
        let selectedBodies = ns.getSelectedBodies()
        let status = document.getElementById("download_status")
        let icon = status.getElementsByTagName("span")[0]
        let limit = document.getElementById("size_limit_selector").value
        let dlc = document.getElementById("include_dlc").checked

        let dlcTypes = dlc ? ["nodlc", "dlc"] : ["nodlc"]
        let count = 0
        for (let body of selectedBodies) {
            for (let category of dlcTypes) {
                if (limit === "") {
                    count += downloadCraftData[body][category]["total"]
                } else {
                    count += downloadCraftData[body][category]["limits"][limit]
                }
            }

        }
        if (count === 0) {
            ns.updateIcon(icon, "minus")
            status.childNodes[1].nodeValue = "0 crafts selected"
        } else {
            ns.updateIcon(icon, "chevron-right")
            status.childNodes[1].nodeValue = `${count} craft(s) selected`
        }
        return count

    }
    this.fetchDownloadDetails = function () {
        let xhttp = new XMLHttpRequest()
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4) {
                let response = ns.processJSONResponse(xhttp)
                if (this.status === 200) {
                    downloadCraftData = response
                    ns.downloadDetails()
                } else {
                    if (response === null) {
                        ns.setBodyOnError(xhttp.response)
                    } else {
                        alert(response["error"])
                    }
                }
            }
        }
        xhttp.open("GET", "/submissions/stats", true)
        xhttp.send()
    }
    this.hookDownloadButtons = function () {
        let container = document.getElementById("craft_checkbox_container")
        let checkboxes = container.querySelectorAll('input[type="checkbox"]')
        let dlc = document.getElementById("include_dlc")
        for (let checkbox of checkboxes) {
            checkbox.addEventListener("change", ns.downloadDetails)
        }
        dlc.addEventListener("change", ns.downloadDetails)


    }
    window.addEventListener("DOMContentLoaded", this.hookDownloadButtons)
    window.addEventListener("DOMContentLoaded", this.fetchDownloadDetails)
}
