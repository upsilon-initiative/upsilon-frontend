# Note about fonts and uikit

These can be acquired at the following locations, and are subject to their own licenses

uikit
- https://github.com/uikit/uikit/releases/

fonts (Trispace and Roboto Mono)
- https://fonts.google.com/

